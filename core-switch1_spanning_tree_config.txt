spanning-tree mode rapid-pvst
no spanning-tree optimize bpdu transmission
spanning-tree extend system-id
spanning-tree portfast default
spanning-tree portfast bpduguard default
spanning-tree etherchannel guard misconfig
spanning-tree loopguard default
spanning-tree priority 32768
